XXXXX challenge

### Deploy Plan
- `cp .env.example .env`
- `composer install`
- `php runnner migrate`
- `php runnner fetchData` (You can run this command in cron job to get new data)
- `php -S localhost:8000 public/index.php`
- Check http://localhost:8000/dashboard to see result

Made with ❤️️
