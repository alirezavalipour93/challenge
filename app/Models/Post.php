<?php

namespace App\Models;

use DateTime;

class Post
{
    static string $tableName = 'posts';

    protected $id;
    protected $from_name;
    protected $from_id;
    protected $message;
    protected $type;
    protected $created_time;


    public function __construct($data)
    {
        $this->setId($data['id']);
        $this->setFromId($data['from_id']);
        $this->setFromName($data['from_name']);
        $this->setMessage($data['message']);
        $this->setType($data['type']);
        $this->setCreatedTime($data['created_time']);
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFromName()
    {
        return $this->from_name;
    }

    /**
     * @param mixed $from_name
     */
    public function setFromName($from_name): void
    {
        $this->from_name = $from_name;
    }

    /**
     * @return mixed
     */
    public function getFromId()
    {
        return $this->from_id;
    }

    /**
     * @param mixed $from_id
     */
    public function setFromId($from_id): void
    {
        $this->from_id = $from_id;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message): void
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getCreatedTime()
    {
        return new DateTime($this->created_time);
    }

    /**
     * @param mixed $created_time
     */
    public function setCreatedTime($created_time): void
    {
        $this->created_time = $created_time;
    }

    /**
     * @return string
     */
    public static function getTableName(): string
    {
        return Post::$tableName;
    }
}