<?php


namespace App\Services\Repositories\Contracts;


use App\Models\Post;

interface PostRepository
{

    /**
     * @param $id
     * @return Post
     */
    public function findById($id): Post;



    public function all();


    /**
     * @param Post $post
     * @return bool
     */
    public function store(Post $post) : bool;



    public function getLastItemDateTime() : string;

    public function averageCharacterLengthOfPostsPerMonth() : array;

    public function longestPostByCharacterLengthPerMonth() : array;

    public function totalPostsSplitByWeekNumber() : array;

    public function averageNumberOfPostsPerUserPerMonth() : array;


}