<?php


namespace App\Services\Repositories\SqlServices;


use App\Models\Post;
use App\Services\Repositories\Contracts\PostRepository;
use App\ApplicationKernel;

class PostService implements PostRepository
{
    private $app;
    private string $tableName;
    private \Doctrine\DBAL\Connection $connection;

    /**
     * PostService constructor.
     * @param ApplicationKernel $app
     */
    public function __construct(ApplicationKernel $app)
    {
        $this->app = $app;
        $this->tableName = Post::getTableName();
        $this->connection = $this->app->getConnection();
    }


    public function findById($id): Post
    {

    }

    public function all()
    {
        $posts = $this->connection
            ->createQueryBuilder()
            ->select("*")
            ->from($this->tableName)
            ->execute()
            ->fetchAllAssociative();

        foreach ($posts as  $key => $rawPost){
           $posts[$key] = new Post($rawPost);
        }

        return $posts;
    }

    public function store(Post $post): bool
    {
       return  $this->connection
            ->createQueryBuilder()
            ->insert($this->tableName)
            ->setValue('id', '?')
            ->setValue('from_name', '?')
            ->setValue('from_id', '?')
            ->setValue('message', '?')
            ->setValue('type', '?')
            ->setValue('created_time', '?')
            ->setParameter(0, $post->getId())
            ->setParameter(1, $post->getFromName())
            ->setParameter(2, $post->getFromId())
            ->setParameter(3, $post->getMessage())
            ->setParameter(4, $post->getType())
            ->setParameter(5, $post->getCreatedTime()->format('Y-m-d\TH:i:s.uP'))
           ->execute();
    }

    public function getLastItemDateTime(): string
    {
       return $this->connection->fetchOne('SELECT created_time from posts ORDER BY created_time DESC');
    }


    public function averageCharacterLengthOfPostsPerMonth(): array
    {
        $connection = $this->app->getConnection();
        $query = 'SELECT  strftime("%Y-%m" , created_time ) as date , CAST(avg(length(message)) as INTEGER) FROM posts GROUP BY strftime("%Y-%m" , created_time )';
        $result = $connection->executeQuery($query)->fetchAllKeyValue();
        return $result;
    }

    public function longestPostByCharacterLengthPerMonth(): array
    {
        $connection = $this->app->getConnection();
        $query = 'SELECT  strftime("%Y-%m" , created_time ) as date , CAST(max(length(message)) as INTEGER) FROM posts GROUP BY strftime("%Y-%m" , created_time )';
        $result = $connection->executeQuery($query)->fetchAllKeyValue();
        return $result;
    }

    public function totalPostsSplitByWeekNumber(): array
    {
        $connection = $this->app->getConnection();
        $query = 'SELECT  strftime("%Y-%W",created_time) as date,CAST(count(*) as INTEGER) FROM posts GROUP BY strftime("%Y-%W" , created_time )';
        $result = $connection->executeQuery($query)->fetchAllKeyValue();
        return $result;
    }

    public function averageNumberOfPostsPerUserPerMonth(): array
    {
        $connection = $this->app->getConnection();
        $query = 'select  date,CAST(avg(counter) as INTEGER) from
                        (SELECT strftime("%Y-%m", created_time) as date, count(from_id) as counter,from_id from posts
                          GROUP BY strftime("%Y-%m", created_time), from_id ) 
                 GROUP BY date';
        $result = $connection->executeQuery($query)->fetchAllKeyValue();
        return $result;
    }
}