<?php

namespace App\Services\Infrastructures\Supermetrics\Concretes\V1;



use App\Services\Infrastructures\Supermetrics\Contracts\AuthenticationInterface;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class AuthenticationService implements AuthenticationInterface
{
    protected string $baseUrl = "https://api.supermetrics.com";
    protected string $username;
    protected string $password;
    protected string $name;
    protected ResponseInterface $response;

    public function getUrl(): string
    {
        return $this->baseUrl . "/assignment/register";
    }

    public function setAuthenticationParameters(string $username, string $password, array $extraParams): AuthenticationService
    {

        $this->username = $username;
        $this->password = $password;
        $this->name = $extraParams['name'] ?? null;
        return $this;
    }

    public function request(): AuthenticationInterface
    {
        $client = new Client();

        $response = $client->post($this->getUrl(), [
            'form_params' => [
                'email' =>  $this->username,
                'client_id' => $this->password,
                'name' => $this->name,
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            throw new Exception("Invalid response from server");
        }

        $this->response = $response;

        return $this;

    }

    public function getResponse(): array
    {
        return json_decode($this->response->getBody()->getContents(), true);
    }

    public function getToken(): string
    {
       return $this->getResponse()['data']['sl_token'] ?? "";
    }


}