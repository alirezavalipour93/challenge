<?php


namespace App\Services\Infrastructures\Supermetrics\Concretes\V1;

use App\Services\Infrastructures\Supermetrics\Contracts\FetchDataInterface;
use Exception;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class FetchDataService implements FetchDataInterface
{
    protected string $baseUrl = "https://api.supermetrics.com";
    private string $token = "";
    private array $headers = [];
    private ResponseInterface $response;

    public function getUrl(): string
    {
        return $this->baseUrl . "/assignment/posts";
    }

    public function setHeaders(array $headers): void
    {
        $this->headers = $headers;
    }

    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    public function connect(string $page): FetchDataInterface
    {
        $client = new Client();

        $response = $client->get($this->getUrl(), [
            'headers' => $this->headers,
            'query' => [
                'page' => $page,
                'sl_token' => $this->token
            ]
        ]);

        if ($response->getStatusCode() != 200) {
            throw new Exception("Invalid response from server");
        }

        $this->response = $response;

        return $this;
    }

    public function getPosts(): array
    {
        return json_decode($this->response->getBody()->getContents() , true)['data']['posts'] ?? [];
    }
}