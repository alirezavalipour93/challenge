<?php

namespace App\Services\Infrastructures\Supermetrics\Contracts;

interface AuthenticationInterface
{

    /**
     * @return string
     */
    public function getUrl(): string;


    /**
     * @param string $username
     * @param string $password
     * @param array $extraParams
     * @return mixed
     */
    public function setAuthenticationParameters(string $username, string $password, array $extraParams): AuthenticationInterface;

    /**
     * @return AuthenticationInterface
     */
    public function request(): AuthenticationInterface;


    /**
     * @return array
     */
    public function getResponse(): array;


    /**
     * @return string
     */
    public function getToken(): string;
}