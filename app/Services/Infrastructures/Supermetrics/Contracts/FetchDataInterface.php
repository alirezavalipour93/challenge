<?php

namespace App\Services\Infrastructures\Supermetrics\Contracts;

/**
 * Interface FetchDataInterface
 */
interface FetchDataInterface
{

    /**
     * @return string
     */
    public function getUrl(): string;

    /**
     * @param array $headers
     */
    public function setHeaders(array $headers): void;

    /**
     * @param string $token
     */
    public function setToken(string $token): void;

    /**
     * @param string $page
     * @return FetchDataInterface
     */
    public function connect(string $page): FetchDataInterface;


    /**
     * @return array
     */
    public function getPosts(): array;

}