<?php


namespace App\Services\Analyzer\Contracts;


use App\ApplicationKernel;

interface AnalyzerInterface
{

    public function __construct(ApplicationKernel $app);

    /*
     *  data so if your Implementation does not need data
     *  or it could read data from database directly set it this to false
     */

    public function needPostsData() : bool;

    /**
     *  Feed posts data in case you need to feed it.
     * @param array $posts
     */

    public function setPostsData(array $posts);

    public function averageCharacterLengthOfPostsPerMonth() : array;

    public function longestPostByCharacterLengthPerMonth() : array;

    public function totalPostsSplitByWeekNumber() : array;

    public function averageNumberOfPostsPerUserPerMonth() : array;
}