<?php


namespace App\Services\Analyzer;


use App\ApplicationKernel;
use App\Services\Analyzer\Contracts\AnalyzerInterface;

class DBEngineAnalyzer implements AnalyzerInterface
{
    public function __construct(ApplicationKernel $app)
    {
        $this->app = $app;
    }

    public function needPostsData(): bool
    {
        return false;
    }

    public function setPostsData(array $posts)
    {

    }

    public function averageCharacterLengthOfPostsPerMonth(): array
    {
        return $this->app->postRepository->averageCharacterLengthOfPostsPerMonth();
    }

    public function longestPostByCharacterLengthPerMonth(): array
    {
        return $this->app->postRepository->longestPostByCharacterLengthPerMonth();
    }

    public function totalPostsSplitByWeekNumber(): array
    {
        return  $this->app->postRepository->totalPostsSplitByWeekNumber();
    }

    public function averageNumberOfPostsPerUserPerMonth(): array
    {
        return $this->app->postRepository->averageNumberOfPostsPerUserPerMonth();
    }
}