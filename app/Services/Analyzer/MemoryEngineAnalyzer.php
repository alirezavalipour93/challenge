<?php


namespace App\Services\Analyzer;

use App\ApplicationKernel;
use App\Services\Analyzer\Contracts\AnalyzerInterface;

/*
 *  You should feed memory analyzer with posts data
 */

class MemoryEngineAnalyzer implements AnalyzerInterface
{

    private array $posts;

    public function __construct(ApplicationKernel $app)
    {
        $this->app = $app;
    }


    /*
     *  Memory Analyzer need to feed by
     *  data so if your Implementation does not need data
     *  or it could read data from database directly set it this to false
     */

    public function needPostsData(): bool
    {
        return true;
    }

    public function setPostsData($posts)
    {
        $this->posts = $posts;
    }

    public function averageCharacterLengthOfPostsPerMonth(): array
    {
        $data = [];
        foreach ($this->posts as $post) {
            $month = $post->getCreatedTime()->format('Y-m');

            if (isset($data[$month]) == false) {
                $data[$month]['total_character'] = 0;
                $data[$month]['total_posts'] = 0;
                $data[$month]['average'] = 0;
            }

            $data[$month]['total_character'] += strlen($post->getMessage());
            $data[$month]['total_posts'] += 1;
            $data[$month]['average'] = (int)($data[$month]['total_character'] / $data[$month]['total_posts']);

        }

        // preparing output data
        $months = [];
        foreach ($data as $key => $month) {
            $months[$key] = $month['average'];
        }

        return $months;
    }

    public function longestPostByCharacterLengthPerMonth(): array
    {
        $data = [];
        foreach ($this->posts as $post) {
            $month = $post->getCreatedTime()->format('Y-m');

            if (isset($data[$month]) == false) {
                $data[$month]['longest_post'] = 0;
            }

            $data[$month]['longest_post'] = max($data[$month]['longest_post'], strlen($post->getMessage()));
        }


        // preparing output data
        $months = [];
        foreach ($data as $key => $month) {
            $months[$key] = $month['longest_post'];
        }

        return $months;

    }

    public function totalPostsSplitByWeekNumber(): array
    {
        $weeks = [];

        foreach ($this->posts as $post) {
            $week = $post->getCreatedTime()->format('Y-W');

            if (isset($weeks[$week]) == false) {
                $weeks[$week] = 0;
            }
            $weeks[$week] += 1;
        }

        return $weeks;

    }

    public function averageNumberOfPostsPerUserPerMonth(): array
    {
        $data = [];
        foreach ($this->posts as $post) {
            $month = $post->getCreatedTime()->format('Y-m');
            $user = $post->getFromId();

            if (isset($data[$month]) == false) {
                $data[$month]['total_posts'] = 0;
                $data[$month]['average_number_of_posts_per_user'] = 0;
            }

            if (isset($data[$month]['user'][$user]['posts']) == false) {
                $data[$month]['user'][$user]['posts'] = 0;
            }

            $data[$month]['user'][$user]['posts'] += 1;
            $data[$month]['total_posts'] += 1;
            $data[$month]['average_number_of_posts_per_user'] =
                (int)($data[$month]['total_posts'] / count($data[$month]['user']));
        }


        // preparing output data
        $months = [];
        foreach ($data as $key => $month) {
            $months[$key] = $month['average_number_of_posts_per_user'];
        }

        return $months;

    }


}