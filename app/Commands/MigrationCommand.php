<?php


namespace App\Commands;


class MigrationCommand extends Command
{

    public function handle()
    {
        echo "Migration runner started \n";
        $schemaManager = $this->app->getConnection()->getSchemaManager();

        if (in_array('posts', $schemaManager->listTableNames()) == false) {
            $this->postsTableMigration();
        }

        echo "Migration Done \n" ;

    }

    public function postsTableMigration(): void
    {
        echo "Migrating posts table \n";

        $schema = new \Doctrine\DBAL\Schema\Schema();
        $myTable = $schema->createTable("posts");
        $myTable->addColumn("id", "string",);
        $myTable->addColumn("from_name", "string");
        $myTable->addColumn("from_id", "string");
        $myTable->addColumn("message", "text");
        $myTable->addColumn("type", "string");
        $myTable->addColumn("created_time", "datetimetz");
        $myTable->setPrimaryKey(array("id"));
        $myTable->addIndex(['from_id']);
        $queries = $schema->toSql($this->app->getConnection()->getDatabasePlatform()); // get queries to create this schema.
        foreach ($queries as $query) {
            $this->app->getConnection()->executeQuery($query);
        }
    }


}