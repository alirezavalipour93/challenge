<?php

namespace App\Commands;


use App\ApplicationKernel;

abstract  class Command
{

    protected ApplicationKernel $app;

    public function __construct()
    {
        $this->app = new ApplicationKernel();
    }

    abstract function handle();


}