<?php


namespace App\Commands;


use App\Models\Post;
use App\Services\Infrastructures\Supermetrics\Contracts\FetchDataInterface;
use DateTime;

class FetchDataFromServerCommand extends Command
{

    public function handle()
    {
        $this->app->setAuthenticationDriver($this->app->authenticationService);
        $this->authenticationProcess();
        $this->fetchDataProcess($this->app->fetchDataService);

    }

    public function authenticationProcess()
    {
        $username = $_ENV['SUPERMETRICS_EMAIL'];
        $password = $_ENV['SUPERMETRICS_CLIENT_ID'];
        $name = $_ENV['SUPERMETRICS_NAME'];

        $this->app->authenticator
            ->setAuthenticationParameters($username, $password, ['name' => $name])
            ->request();
    }

    public function fetchDataProcess(FetchDataInterface $fetchDataService)
    {
        $pages = range(1, 10);
        $token = $this->app->authenticator->getToken();
        $fetchDataService->setToken($token);


        foreach ($pages as $page) {
            $posts = $fetchDataService->connect($page)->getPosts();

            $cachedLastPostDateTime = $this->app->postRepository->getLastItemDateTime();
            $postLastDateTime = new DateTime($cachedLastPostDateTime);
            $lastPostDateTimeFromServer = new DateTime($posts[0]['created_time']);

            if ($postLastDateTime == $lastPostDateTimeFromServer) {
                echo "You have latest version of data at:  $cachedLastPostDateTime \n";
                break;
            }


            echo "\n Fetching data of page:$page ";

            foreach ($posts as $key => $post) {
                try{
                    $this->app->postRepository->store(new Post($post));
                }catch (\Exception $exception){
                    echo $exception->getMessage();
                }

            }

            // not to reach maximum request amount of server
            usleep(500 * 1000);
        }
    }


}