<?php


namespace App\Controllers;


use App\ApplicationKernel;
use App\Services\Analyzer\Contracts\AnalyzerInterface;

class DashboardController
{

    private ApplicationKernel $app;

    public function __construct(ApplicationKernel $app)
    {
        $this->app = $app;
    }

    public function dashboard()
    {
        $data = [];

        if ($this->app->analyzerService->needPostsData() == true){
            $this->app->analyzerService->setPostsData($this->app->postRepository->all());
        }

        $data['average_character_length_of_posts_per_Month'] = $this->app->analyzerService->averageCharacterLengthOfPostsPerMonth();
        $data['longest_post_by_character_length_per_month'] = $this->app->analyzerService->longestPostByCharacterLengthPerMonth();
        $data['total_posts_split_by_week_Number'] = $this->app->analyzerService->totalPostsSplitByWeekNumber();
        $data['average_number_of_posts_per_user_per_month'] = $this->app->analyzerService->averageNumberOfPostsPerUserPerMonth();

        return json_encode($data , JSON_PRETTY_PRINT);
    }
}