<?php

namespace App;

use App\Controllers\DashboardController;
use App\Services\Analyzer\Contracts\AnalyzerInterface;
use App\Services\Analyzer\DBEngineAnalyzer;
use App\Services\Analyzer\MemoryEngineAnalyzer;
use App\Services\Infrastructures\Supermetrics\Concretes\V1\AuthenticationService;
use App\Services\Infrastructures\Supermetrics\Concretes\V1\FetchDataService;
use App\Services\Infrastructures\Supermetrics\Contracts\AuthenticationInterface;
use App\Services\Infrastructures\Supermetrics\Contracts\FetchDataInterface;
use App\Services\Repositories\Contracts\PostRepository;
use App\Services\Repositories\SqlServices\PostService;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use Dotenv\Dotenv;
use Exception;

class ApplicationKernel
{

    public AuthenticationInterface $authenticator;
    public array $data;
    public Connection $connection;
    public AnalyzerInterface $analyzerService;
    public PostRepository $postRepository;
    public AuthenticationInterface $authenticationService;
    public FetchDataInterface $fetchDataService;


    public function __construct()
    {
        $this->setEnvironmentVariable();
        $this->setDatabaseConnection();
        $this->classBinder();
    }

    public function setAuthenticationDriver(AuthenticationInterface $authenticationService)
    {
        $this->authenticator = $authenticationService;
    }

    /**
     * @return Connection
     */
    public function getConnection(): Connection
    {
        return $this->connection;
    }


    public function router()
    {
        $uri = $_SERVER['REQUEST_URI'];

        if ($uri == '/dashboard') {
            return (new DashboardController($this))->dashboard();
        }
    }

    public function setDatabaseConnection(): void
    {
        $connectionParams = array(
            'url' => "sqlite:///" . __DIR__ . "/../storage/db/SUPERMETRICS_DB.sqlite",
        );

        $this->connection = DriverManager::getConnection($connectionParams);
    }

    public function setEnvironmentVariable(): void
    {
        $dotenv = Dotenv::createImmutable(__DIR__ . '/..');
        $dotenv->load();
        $dotenv->required([
            'SUPERMETRICS_CLIENT_ID',
            'SUPERMETRICS_EMAIL',
            'SUPERMETRICS_NAME'
        ]);
    }

    public function classBinder(): void
    {
        $this->authenticationService = new AuthenticationService();
        $this->fetchDataService = new FetchDataService();
        $this->postRepository = new PostService($this);

        /*
         * you can choose analyzing data with PHP or SQL
         * For using PHP bind MemoryEngineAnalyzer
         * For using SQL bind DBEngineAnalyzer
         */
//        $this->analyzerService = new MemoryEngineAnalyzer($this);
        $this->analyzerService = new DBEngineAnalyzer($this);

        $this->setAuthenticationDriver($this->authenticationService);
    }

}